


                     eT 1.7 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   E. RoncaM. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. 
   H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.7.0 Hydra
  ------------------------------------------------------------
  Configuration date: 2022-04-07 12:33:31 UTC -07:00
  Git branch:         release-v1.7.0
  Git hash:           c5c382d7d23365463896a87c37298e4a37e4c962
  Fortran compiler:   GNU 11.2.0
  C compiler:         GNU 11.2.0
  C++ compiler:       GNU 11.2.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-04-07 12:34:38 UTC -07:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF He
        charge: 0
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
     end method


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866813438575     0.601441650843     5.000010253959        1
        2  F    -0.866813438575     0.601441650843     5.000010253959        2
        3  O     0.000000000000    -0.075788760148     4.999962628010        3
        4 He     0.000000000000     0.000000000000     7.500028610369        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638040000000     1.136560000000     9.448650000000        1
        2  F    -1.638040000000     1.136560000000     9.448650000000        2
        3  O     0.000000000000    -0.143220000000     9.448560000000        3
        4 He     0.000000000000     0.000000000000    14.173000000000        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               38
     Number of orthonormal atomic orbitals:   38

  - Molecular orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         28
     Number of molecular orbitals:       38


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:              -178.316344758164
     Number of electrons in guess:           20.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -177.452445750079     0.7565E-01     0.1775E+03
     2          -177.479041706178     0.1275E-01     0.2660E-01
     3          -177.480036278004     0.4170E-02     0.9946E-03
     4          -177.480163949507     0.1388E-02     0.1277E-03
     5          -177.480177943495     0.4302E-03     0.1399E-04
     6          -177.480179297195     0.7948E-04     0.1354E-05
     7          -177.480179380363     0.2291E-04     0.8317E-07
     8          -177.480179390696     0.4810E-05     0.1033E-07
     9          -177.480179391527     0.1046E-05     0.8309E-09
    10          -177.480179391558     0.2394E-06     0.3087E-10
    11          -177.480179391559     0.5245E-07     0.9663E-12
    12          -177.480179391559     0.1001E-07     0.2842E-13
    13          -177.480179391558     0.2797E-08     0.5684E-13
    14          -177.480179391558     0.1443E-08     0.2842E-13
    15          -177.480179391559     0.6299E-09     0.2274E-12
    16          -177.480179391558     0.1436E-09     0.1990E-12
    17          -177.480179391558     0.4658E-10     0.0000E+00
  ---------------------------------------------------------------
  Convergence criterion met in 17 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.600136689052
     Nuclear repulsion energy:      48.518087408280
     Electronic energy:           -225.998266799839
     Total energy:                -177.480179391558

  - Timings for the RHF ground state calculation

     Total wall time (sec):              1.12800
     Total cpu time (sec):               1.03032

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 307.628 KB

  Total wall time in eT (sec):              1.14100
  Total cpu time in eT (sec):               1.04290

  Calculation ended: 2022-04-07 12:34:39 UTC -07:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
